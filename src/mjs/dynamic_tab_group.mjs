import {Element} from "./element.mjs";
import * as util from "./util.mjs";

export class DynamicTabGroup extends Element {
    constructor() {
        super('cs_dynamic_tab_group');
    }

    connectedCallback() {
        const targetSelector = `#${this.dataset.targetId}`;
        const searchRoot = this.parentNode.getRootNode().host._root ?
               this.parentNode.getRootNode().host._root
               : this.parentNode.getRootNode().host.shadowRoot;

        console.assert(searchRoot, 'no search root');
        if (!searchRoot) {
            this._root.innerHTML = "[no search root]";
            return;
        }

        this._target = searchRoot.querySelector(targetSelector);
        console.assert(this._target, 'no target')
        if (!this._target) {
            this._root.innerHTML = "[no target]";
            return;
        }

        this._target.addEventListener('change', e => {
            // Ignore any other bubbling+composed events from within the target
            if (e.target !== this._target) {
                return;
            }

            console.log('TARGET CHANGE');
            const instanceState = e.target.getInstanceState();
            console.log('instance state', instanceState);
        });

        super.connectedCallback();
    }

    // propagatePageState() {
    //     if (this._pageManager) {
    //         const displayPage = this._pageManager._currentPage.getLinkStr('tabto');
    //         const displayStack = [];
    //         for (const stackPage of this._pageManager._pageStack) {
    //             displayStack.push(stackPage.getLinkStr('tabto'));
    //         }
    //         this.display = {
    //             'page': displayPage,
    //             'stack': displayStack,
    //         }
    //         this._renderDynamicContent();
    //     }
    //     super.propagatePageState();
    // }
};
window.customElements.define('cs-dynamic-tab-group', DynamicTabGroup);