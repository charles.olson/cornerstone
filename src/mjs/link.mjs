import {global, ElementMixIn, Element} from "./element.mjs";
import * as util from "./util.mjs";

export class Link extends ElementMixIn(HTMLAnchorElement) {
    constructor(template) {
        super();

        this.href = '';

        this.addEventListener('click', e => {
            this.href = this.getHref();
            history.pushState(null, null, this.href);
            window.dispatchEvent(new HashChangeEvent("hashchange"));
            e.preventDefault();
        });

        this.addEventListener('contextmenu', e => {
            this.href = this.getHref(false);
        });
    }

    set activeFilter(v) {
        super.activeFilter = v;
        this.href = this.getHref(false);
    }

    getHref() {
        // Make a copy of the globalPageState (to modify)
        const mergedFilter = util.deepCopy(global.app.pageState);
        util.setPathValueArray(mergedFilter, this._pageContext, this._activeFilter);
        console.log('Link', this._pageContext, this._activeFilter, mergedFilter);
        const hrefStr = util.pageStateToHashStr(mergedFilter);
        return `#${hrefStr}`;
    }
}
window.customElements.define('cs-link', Link, {extends: 'a'});


export class LocalLink extends ElementMixIn(HTMLElement) {
    static get observedAttributes() {
        return [...super.observedAttributes, 'link-action'];
    }

    attributeChangedCallback(name, oldVal, newVal) {
        super.attributeChangedCallback(name, oldVal, newVal);
        if (name === 'link-action') {
            this._linkAction = newVal.toLowerCase();
        }
    }

    connectedCallback() {
        super.connectedCallback();
        if (this.hasAttribute('active')) {
            this.removeAttribute('active');
            this._execAction();
        }
    }

    constructor(template) {
        super();

        this._linkAction = 'add';
        this.addEventListener('click', e => {
            this._execAction();
            e.preventDefault();
        });
    }

    _execAction() {
        if (this._linkAction === 'add') {
            const newState = {...this._pageStateRoot.internalPageState, ...this._activeFilter};
            this._pageStateRoot.internalPageState = newState;
        }
        else {
            // toggle
            // remove
            assert(false, `${this._linkAction} action not implemented yet`);
        }
    }
}
window.customElements.define('cs-local-link', LocalLink);