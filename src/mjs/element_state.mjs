export class ElementState {
    constructor(key, index, stateMachine, config, track) {
        this._key = key;
        this._index = index;
        this._stateMachine = stateMachine;
        this._config = config;
        this._track = track;
        this._startTimeMS = undefined;
        this._totalTimeMS = 0;
        this._startCount = 0;
    }

    set local(jsonVal) {
        this._stateMachine.local = jsonVal;
    }

    set dynamic(jsonVal) {
        this._stateMachine.dynamic = jsonVal;
    }

    set value(v) {
        this._stateMachine.value = v;
    }

    get value() {
        return this._stateMachine.value;
    }

    get shared() {
        return this._stateMachine._shared;
    }

    get global() {
        return this._stateMachine.global;
    }

    get el() {
        return this._stateMachine.el;
    }

    async _start() {
        console.log('STATE START', this._key);
        this._startCount++;
        this._startTimeMS = Date.now();
        await this.start();
    }

    async _update() {
        console.log('STATE UPDATE', this._key);
        await this.update();
    }

    async _stop() {
        const elapsedTimeMS = Date.now() - this._startTimeMS;
        this._totalTimeMS += elapsedTimeMS;
        console.log('STATE STOP', this._key, elapsedTimeMS);
        await this.stop();
    }

    _execFunction(fnName, eventTarget) {
        if (this[fnName] === undefined) {
            return false;
        }

        // Trigger the function by name, pass in the instigator
        const fn = this[fnName].bind(this);
        fn(eventTarget);
        return true;
    }

    getStats() {
        return {
            'start_count': this._startCount,
            'total_time_ms': this._totalTimeMS,
        }
    }
    
    //
    // StateMachine commands
    //

    spawn(key) {
        if (Array.isArray(key)) {
            for (k of key) {
                this._stateMachine.spawnState(k);
            }
        }
        else {
            this._stateMachine.spawnState(key);
        }
    }

    kill() {
        this._stateMachine.killState(this._key);
    }

    goto(key) {
        this._stateMachine.killState(this._key);
        this._stateMachine.spawnState(key);
    }

    killAll(track=undefined) {
        this._stateMachine.killAllStates(track);
    }

    //
    // Virtual methods
    //

    async start() { }
    async update() { }
    async stop() { }
}


export class TestPromiseElementState extends ElementState {
    async start() {
        this.test = test.testPromise(this._config.delay).then(() =>
            this.goto(this._config.next)
        );
        console.log('ASYNC TEST');
    }
}