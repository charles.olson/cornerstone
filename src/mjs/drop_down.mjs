import {Element} from "./element.mjs"

export class DropDown extends HTMLElement {
    constructor(template) {
        super();

        this.el = {};
        this.el.title = this.querySelector('cs-drop-down-title');
        this.el.content = this.querySelector('cs-drop-down-content');

        this.addEventListener('mouseenter', e => {
            this.show();
        });
        this.addEventListener('mouseleave', e => {
            this.hide();
        });

        // Toggle on click
        this.el.title.addEventListener('click', e => {
            this.toggle();
        });
    }

    connectedCallback() {
        // Optional window click listener to close on external click
        if (!this.hasAttribute('manual-close')) {
            document.addEventListener('click', e => {
                const clickPath = e.composedPath();
                for (const el of clickPath) {
                    const isSelf = (el === this);
                    if (isSelf) {
                        return;
                    }
                }
                this.forceHide();
            });
        }
    }

    show() {
        const contentLocation = this.getAttribute('content-location');
        const rect = this.getBoundingClientRect();

        // Render first to get the width of the content, then position it
        this.el.content.classList.add('active');

        const contentWidth = this.el.content.scrollWidth;

        switch(contentLocation) {
            case 'right':
                this.el.content.style.left = rect.right + 'px';
                this.el.content.style.top = rect.top + 'px';
                break;
            case 'left':
                this.el.content.style.left = (rect.left - contentWidth) + 'px';
                this.el.content.style.top = rect.top + 'px';
                break;
            case 'bottom-left':
                this.el.content.style.left = (rect.right - contentWidth) + 'px';
                this.el.content.style.top = rect.bottom + 'px';
                break;
            case 'bottom':
            default:
                this.el.content.style.left = rect.left + 'px';
                this.el.content.style.top = rect.bottom + 'px';
        }
    }

    hide() {
        if (this._on) {
            return;
        }
        this.el.content.classList.remove('active');
    }

    forceHide() {
        this._on = false;
        this.classList.remove('toggled-on');
        this.el.content.classList.remove('active');
    }

    toggle() {
        this._on = !this._on;
        if (this._on) {
            this.show();
            this.classList.add('toggled-on');
        }
        else {
            this.classList.remove('toggled-on');
            this.hide();
        }
    }
}
window.customElements.define('cs-drop-down', DropDown);