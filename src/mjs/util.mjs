export function isObject(v) {
    if (v === null) return true;
    if (Array.isArray(v)) return true;
    if (typeof(v) === 'object') return true;

    return false;
}


export function matchFilter(state, posFilter) {
    console.assert(state, 'matchFilter: no state');
    if (!state) {
        return false;
    }

    if (Object.keys(state).length == 0) {
        //console.log('empty state');
        return false;
    }
    if (posFilter === null || posFilter === undefined) {
        //console.log('empty filter');
        return true;
    }

    if (typeof(state) !== typeof(posFilter)) {
        //console.error('state filter mismatch');
        return false;
    }

    /// Check if the KVPs in posFilter all appear in the currentState
    ///  posFilter RHS can be an array of acceptable values
    for(const[key, val] of Object.entries(posFilter)) {
        //const currentVal = currentState[key];
        if (val === '*') {
            // Allow wildcard; mainly so modals can clear parameters in getDeactivateFilter
            continue;
        }
        const currentVal = getPathValue(state, key);
        if (Array.isArray(val)) {
            if (!val.includes(currentVal)) return false;
        }
        if (typeof(val) === 'object') {
            if (!matchFilter(val, currentVal)) {
                return false;
            }
        }
        else {
            if (currentVal !== val) return false;
        }
    }
    return true;
}


export function deepCopy(obj) {
    const strVal = JSON.stringify(obj);
    const jsonVal = JSON.parse(strVal);
    return jsonVal;
}


export function deepEquals(objA, objB, ignoreKeys=undefined) {
    /// Deep value equality comparison of any type
    if (objA === null) return (objB === null);
    if (objB === null) return (objA === null);
    if (objA === undefined) return (objB === undefined);
    if (objB === undefined) return (objA === undefined);

    const objTypeA = typeof(objA);
    const objTypeB = typeof(objB);
    if (objTypeA !== objTypeB) {
        return false;
    }
    else if (objTypeA === 'object') {
        const keysA = Object.keys(objA);
        const keysB = Object.keys(objB);

        if (ignoreKeys) {
            if (Array.isArray(ignoreKeys)) {
                for (const ignoreKey of ignoreKeys) {
                    removeValueFromArray(ignoreKey, keysA);
                    removeValueFromArray(ignoreKey, keysB);
                }
            }
            else {
                removeValueFromArray(ignoreKeys, keysA);
                removeValueFromArray(ignoreKeys, keysB);
            }
        }

        if (keysA.length !== keysB.length) {
            return false;
        }

        const allKeys = [...keysA, ...keysB].filter(uniqueFilter);
        if (allKeys.length !== keysA.length) {
            return false;
        }
        
        for (let k=0; k<allKeys.length; k++) {
            const key = allKeys[k];
            const subA = objA[key];
            const subB = objB[key];
            if (!deepEquals(subA, subB)) {
                return false;
            }
        }
    }
    else if (objTypeA === 'array') {
        if (objA.length !== objB.length) {
            return false;
        }
        for (let a=0; a<objA.length; a++) {
            const itemA = objA[a];
            const itemB = objB[a];
            if (!deepEquals(itemA, itemB)) {
                return false;
            }
        }
    }
    else {
        const c = (objA === objB);
        return c;
    }
    return true;
}


function uniqueFilter(value, index, self) {
    return self.indexOf(value) === index;
}


function removeValueFromArray(val, array, all=false) {
    let index = array.indexOf(val);
    while (index > -1) {
        array.splice(index, 1);
        if (all) index = array.indexOf(val);
    }
    return array; // Optional
}


export function getPathValue(obj, pathStr, defaultVal=undefined) {
    if (!obj) {
        return defaultVal;
    }

    if (!pathStr) {
        return obj;
    }

    let val = obj;
    const pathList = pathStr.split('.');

    for (let p=0; p<pathList.length; p++) {
        const key = pathList[p];
        val = val[key];
        if (val === undefined) {
            return defaultVal;
        }
    }

    return val;
}


export function setPathValue(obj, pathStr, val) {
    if (!obj) {
        return undefined;
    }
    if (!pathStr) {
        // for (const[key, subval] of Object.entries(val)) {
        //     obj[key] = subval;
        // }
        // return obj;
        return Object.assign(obj, val);
    }

    const pathArray = pathStr.split('.');
    return setPathValueArray(obj, pathArray, val);
}


export function setPathValueArray(obj, pathArray, val) {
    if (!pathArray) {
        pathArray = [];
    }

    let node = obj;
    const pathLength = pathArray.length;

    for (let p=0; p<pathLength-1; p++) {
        const key = pathArray[p];
        let nextNode = node[key];
        if (nextNode === undefined) {
            nextNode = {};
            node[key] = nextNode;
        }
        node = nextNode;
    }

    if (pathLength > 0) {
        const finalKey = pathArray[pathLength-1];
        node[finalKey] = val;
    }
    else {
        //console.assert(typeof(val) === 'object', 'Attempting to assign non-object at root of object');
        Object.assign(obj, val);
    }

    return obj;
}


// export function getOrSetPathValue(obj, pathStr, createFn) {
// }


export function parseStrVal(strVal) {
    if (typeof(strVal) !== 'string') {
        return strVal;
    }

    try {
        return JSON.parse(strVal);
    }
    catch(e) {
    }

    const floatVal = parseFloat(strVal);
    if (!isNaN(floatVal)) {
        return floatVal;
    }

    const lVal = strVal.toLowerCase();
    if (lVal === 'null') return null;
    if (lVal === 'true') return true;
    if (lVal === 'false') return false;

    return strVal;
}


export function getElementValue(el) {
    if (el.matches('[type="checkbox"]')) {
        return el.checked;
    }
    return parseStrVal(el.value);
}


export function setElementValue(el, val) {
    if (el.matches('[type="checkbox"]')) {
        el.checked = val;
    }
    else {
        el.value = val;
    }
}


export function strToJsonSafe(val) {
    try {
        return JSON.parse(val);
    }
    catch(e) { }
    return val;
}


export function JsonToUrlEncodedStr(obj) {
    var str = '';
    var hasPrev = false;
    for (const [key, val] of Object.entries(obj)) {
        // Omit null values altogether
        if (val === null) {
            continue;
        }

        if (hasPrev) str += '&';
        else hasPrev = true;

        // Either convert the object to a string, otherwise encode the string directly
        const dataType = typeof(val);
        const strVal = (dataType === 'object' || dataType === 'array')
                    ? encodeURIComponent(JSON.stringify(val))
                    : encodeURIComponent(val);
        str += `${key}=${strVal}`;
    }
    return str;
}


export function urlEncodedStrToJson(strVal) {
    if (strVal.length > 2) {
        const jsonStr = '{"' + strVal.replace(/&/g, '","').replace(/=/g,'":"') + '"}';
        const jsonData = JSON.parse(
            jsonStr,
            function(key, value) {
                // return top-level value
                if (key === '') return value;

                const strValue = decodeURIComponent(value);
                return strToJsonSafe(strValue);
            }
        );
        return jsonData;
    }
    return {};
}


/// Clean up a url string (trim trailing slash) and append query params
export function toUrl(urlStr, params) {
    if (!urlStr) urlStr = '';

    // Remove the leading and trailing slash (potentially results in empty string)
    //let strVal = '/' + urlStr.replaceAll(/^\/|\/$/g, '');
    //if (strVal === '') strVal = '/';

    // Append the encoded parameters
    if (params) {
        urlStr += '?' + JsonToUrlEncodedStr(params);
    }

    return urlStr;
};


export function kvpObjToArray(obj) {
    const maxIndex = Math.max(...Object.keys(obj));
    const returnArray = Array(maxIndex + 1);
    for (const [key, val] of Object.entries(obj)) {
        returnArray[parseInt(key)] = val;
    }
    return returnArray;
}


export function pageStackToHashStr(pageStack) {
    //const hashStr = btoa(JsonToUrlEncodedStr(pageStack));
    const hashStr = JsonToUrlEncodedStr(pageStack);
    return hashStr;
}


export function hashStrToPageStack(hashStr) {
    //const stateStackDict = urlEncodedStrToJson(atob(hashStr));
    const pageStackDict = urlEncodedStrToJson(hashStr);
    const pageStack = kvpObjToArray(pageStackDict);
    return pageStack;
}


export function pageStateToHashStr(pageState) {
    if (pageState === undefined || pageState === null) {
        pageState = {};
    }
    const hashStr = JsonToUrlEncodedStr(pageState);
    return hashStr;
}


export function hashStrToPageState(hashStr) {
    const pageState = urlEncodedStrToJson(hashStr);
    return pageState;
}


export async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
