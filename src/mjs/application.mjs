import {StateMachine, defaultStateMachineDef} from "./state_machine.mjs";
import {ElementState} from "./element_state.mjs";
import {global, Element} from "./element.mjs";
import * as util from "./util.mjs";

const defaultPageState = {};

export class Application extends StateMachine {
    constructor(stateMachineDef, defaultPageState) {
        if (!stateMachineDef) {
            stateMachineDef = defaultStateMachineDef;
        }

        super(stateMachineDef);
        global.app = this;

        this._defaultPageState = defaultPageState ? defaultPageState : {};

        window.addEventListener('resize', () => {
            this.onWindowResize();
        });
        //this.onWindowResize();

        window.addEventListener('hashchange', () => {
            this.syncPageState();
        });

        // Load the initial page state from the location
        this.syncPageState(true);

        // Load the local options, like theme, font mods, etc
        Element.initLocalOptions();
    }

    get state() {
        return this._activeStateMap;
    }

    getTitleFromPageState(pageState) {
        console.error('getTitleFromPageState not implemented by application');
        return 'NO PAGE NAME';
    }

    onWindowResize() {
        //const isTouch = ('ontouchstart' in document.documentElement && navigator.userAgent.match(/Mobi/));
        // TODO: save the window dimensions

        // this._resizeRequested = true;
        // if (!this._resizeTimeout) {
        //     this._updateWindowResize();
        //     this._resizeTimeout = setTimeout(() => this._updateWindowResize(), 250);
        // }
    }

    // _updateWindowResize() {
    //     this._resizeTimeout = null;
    //     if (this._resizeRequested) {
    //         this._resizeRequested = false;
    //         this._isMobile = ('ontouchstart' in document.documentElement && navigator.userAgent.match(/Mobi/));
    //         if (this._rendered) {
    //             this._render(true);
    //         }
    //     }
    // }

    getPageStateFromLocation() {
        const hashStr = document.location.hash.substring(1);
        if (hashStr) {
            const pageState = util.hashStrToPageState(hashStr);

            // if (pageState.section) {
            //     return pageState;
            // }
            return pageState;
        }

        return defaultPageState;
    }

    syncPageState(init=false) {
        const pageState = this.getPageStateFromLocation();
        const mergedPageState = Object.assign({}, this._defaultPageState, pageState);
        this._pageState = mergedPageState;

        if (!init) {
            this._renderDynamicContent();
        }
        // if (init) {
        //     pageState._s = 0;
        // }
        // this._currentPage.pageState = pageState;
        // console.log('SYNC PAGE STATE', this._currentPage);
    }
}
