// Use global commonJS jotun-client until mjs version is ready
//import * as Jotun2 from "@keyrock-dev/jotun-client";
//import * as hb from 'handlebars';

import * as util from "./util.mjs";

const _cssMap = {};
var _cachedHeaderHTML = '';

class GlobalObject {
    constructor() {
        this._app = null;
        this._user = null;
    }
    set app(v) {
        this._app = v;
    }
    get app() {
        return this._app;
    }
}
export var global = new GlobalObject();


export const ElementMixIn = (Base) => class extends Base {
    static get observedAttributes() {
        return ['active-filter'];
    }

    connectedCallback() {
        this._internalPageState = {};
        this._externalPageState = {};
        this._pageStateRoot = this.getRootNode().host;
        this._pageContext = this._pageStateRoot ? this._pageStateRoot.pageContext : [];
        this._pageState = this._pageStateRoot ? this._pageStateRoot.pageState : global.app.pageState;
        this._connected = true;
        if (!this.hasAttribute('data-bind-page-state')) {
            this.setAttribute('data-bind-page-state', '');
        }
        this.updateActive();
    }

    disconnectedCallback() {
        this._connected = false;
    }

    attributeChangedCallback(name, oldVal, newVal) {
        if (name === 'active-filter') {
            this.activeFilter = newVal ? JSON.parse(newVal) : null;
            this.updateActive();
        }
    }

    evalFilter(filter) {
        const isMatch = util.matchFilter(this._pageState, filter);
        console.assert(this._pageState, 'Element missing pageState:', this);
        //console.log('EL: evalFilter', this._pageState, filter, isMatch, this);
        return isMatch;
    }

    updateActive() {
        if (!this._connected) {
            return;
        }

        this.active = this.evalFilter(this._activeFilter);
    }

    get global() {
        return global;
    }

    set active(val) {
        if (val !== this._active) {
            this._active = val;
            if (val) {
                this.onActivate();
            }
            else {
                this.onDeactivate();
            }
        }
    }

    get active() {
        return this._active;
    }

    set activeFilter(v) {
        this._activeFilter = v;
    }

    get activeFilter() {
        return this._activeFilter;
    }

    set internalPageState(jsonVal) {
        const unchanged = util.deepEquals(this._internalPageState, jsonVal);
        if (unchanged) return;

        this._internalPageState = jsonVal;

        this._pageState = {...this._externalPageState, ...this._internalPageState};
        this.updateActive();
        this._updatePageStateElements();
    }

    get internalPageState() {
        return this._internalPageState;
    }

    set pageState(jsonVal) {
        const unchanged = util.deepEquals(this._externalPageState, jsonVal);
        if (unchanged) return;

        this._externalPageState = jsonVal;

        this._pageState = {...this._externalPageState, ...this._internalPageState};
        this.updateActive();
        this._updatePageStateElements();
    }

    _updatePageStateElements() {
        // Do nothing unless it has a shadow dom
    }

    get pageState() {
        return this._pageState;
    }

    set pageContext(v) {
        this._pageContext = v;
    }

    get pageContext() {
        return this._pageContext;
    }

    onActivate() {
        console.assert(this._connected, 'activated disconnected element');
        this.classList.add('active');
    }

    onDeactivate() {
        this.classList.remove('active');
    }
};


export class Element extends ElementMixIn(HTMLElement) {
    static registerCSS(key, cssFile) {
        _cssMap[key] = cssFile;

        _cachedHeaderHTML = '';
        for (const[key, cssFile] of Object.entries(_cssMap)) {
            _cachedHeaderHTML += `<link rel="stylesheet" href="${cssFile}" />`;
        }
    }

    static get observedAttributes() {
        return [...super.observedAttributes, 'value', 'template'];
    }

    attributeChangedCallback(name, oldVal, newVal) {
        super.attributeChangedCallback(name, oldVal, newVal);

        if (name === 'value') {
            const parsedVal = util.parseStrVal(newVal);
            this.value = parsedVal;
        }
        else if (name === 'template') {
            this.template = newVal;
        }
        else {
            //this._root.querySelectorAll(`[data-sync-attribute="${name}"]`).forEach((el) => {el.value = newVal;});
        }
    }

    constructor(template) {
        super();

        this.template = template;

        this._connected = false;
        this._rendered = false;

        this._local = undefined;
        this._value = undefined;
        this._dynamic = undefined;

        this._active = undefined;
        this._activeFilter = undefined;

        // Keep a copy of the original content
        this._originalText = this.innerHTML;
        this._slotChildren = [];
        for (const el of this.children) {
            this._slotChildren.push(el);
        }

        this.innerHTML = '';
        this._root = this.attachShadow({mode: 'open'});
        this._registerListeners();
    }

    _registerListeners() {
        // Shortcut for binding onClick functions (without doing "this.getRootNode().host.[function]")
        this._root.addEventListener('click', (event) => this._handleClickEvent(event));
        this._root.addEventListener('change', (event) => this._handleChangeEvent(event));
    }

    _handleClickEvent(event) {
        if (event.target.hasAttribute('click-function')) {
            const fnName = event.target.getAttribute('click-function');
            this._execFunction(fnName, event.target);
        }

        // // Activate local state
        // if (event.target.hasAttribute('click-activate-local')) {
        //     const localFilterStr = event.target.getAttribute('local-filter');
        //     const localFilter = JSON.parse(localFilterStr);
        //     self.activateLocalFilter(localFilter);
        // }

        // // Toggle additive local state
        // if (event.target.hasAttribute('click-toggle-local-group')) {
        //     // Sync the active state of all local elements in this group
        //     const shouldBeActive = !event.target._isActive;
        //     const localGroup = event.target.getAttribute('local-group');
        //     self.setLocalGroup(localGroup, shouldBeActive);
        // }

        // // Toggle global group state
        // // TODO: This is a little hacky, since it isn't replicated up to all components
        // // But is mainly intended for unique local state keys that should survive save & load of settings
        // if (event.target.hasAttribute('click-toggle-global-group')) {
        //     const globalGroup = event.target.getAttribute('global-group');
        //     const shouldBeActive = !FA.groupState[globalGroup];
        //     self.setGlobalGroup(globalGroup, shouldBeActive);
        // }
    }

    _handleChangeEvent(event) {
        if (event.target.hasAttribute('change-function')) {
            const fnName = event.target.getAttribute('change-function');
            this._execFunction(fnName, event.target);
        }
    }

    _execFunction(fnName, target) {
        // Trigger the function by name, pass in the instigator
        console.assert(this[fnName], `${fnName} is not a member of`, this);
        const fn = this[fnName].bind(this);
        fn(target);
    }

    connectedCallback() {
        super.connectedCallback();
        this.setAttribute('is-cs-element', '');

        // Normal flow is to render when first activated
        //  but if rendered content needs to be scraped or crawled,
        //  then activate (and render) immediately on connect
        //console.warn('TODO: Handle renderOnConnect');
        // if (global.app.settings.renderOnConnect) {
        //     console.log('RENDER ON CONNECT');
        //     this.update();
        // }
    }

    set template(t) {
        if (this._template !== t) {
            this.setTemplateAndRender(t);
        }
    }

    setTemplateAndRender(t) {
        this._template = t;
        if (this._rendered) {
            // Re-render all content
            this._rendered = false;
            this._renderStaticContent();
        }
    }

    set dynamic(jsonVal) {
        if (!util.deepEquals(jsonVal, this._dynamic)) {
            this._dynamic = jsonVal;
            if (this._rendered) {
                this._renderDynamicContent();
            }
        }
    }

    set value(jsonVal) {
        if (!util.deepEquals(jsonVal, this._value)) {
            this._value = jsonVal;
            if (this._rendered) {
                this._renderDynamicContent();
            }
        }
    }

    get value() {
        if (!this._rendered) {
            return this._value;
        }

        return this.scrapeValue();
    }

    get root() {
        return this._root;
    }

    scrapeValue() {
        if (!this._rendered) {
            console.error('scrapeValue: element not yet rendered', this);
            return undefined;
        }

        const v = {};
        this._root.querySelectorAll('[data-bind-value]').forEach((el) => {
            const val = util.getElementValue(el);
            if (val !== undefined) {
                util.setPathValue(v, el.dataset.bindValue, val);
            }
        });
        return v;
    }

    get context() {
        // return {
        //     // TODO: Clean this up; refactoring element
        //     // to static, dynamic, etc
        //     'data': this.dataset,
        //     'value': this._value,
        //     'self': this,
        //     'app': global.app.getRenderContext(),
        // };
        return {
            // Subscribe to global data change?
            //  trigger events when state or page change
            // on static change, have to re-render entire element
            // on dynamic change, only have to update bound data
            // on value change, etc
            // set up events and listeners then?
            // static (needs a different name -- maybe local)

            // Static (only updates via re-render)
            'global': global,

            // Dynamic (updates via data-bind-template or data-bind-value)
            'value': this._value,
            'dynamic': this._dynamic,
        }
    }

    get visible() {
        return (this.offsetWidth && this.offsetHeight);
    }

    onActivate() {
        super.onActivate();

        if (!this._rendered) {
            // Render the structural HTML
            this._renderStaticContent();
        }

        const e = new Event('activate', {bubbles: true, composed: false});
        this.dispatchEvent(e);
    }

    onDeactivate() {
        super.onDeactivate();

        const e = new Event('deactivate', {bubbles: true, composed: false});
        this.dispatchEvent(e);
    }

    _renderStaticContent() {
        const finalTarget = this._root;
        const finalTemplate = this._template;

        //console.log(`EL: renderStaticContent`, finalTemplate, this);
        console.assert(!this._rendered, 'EL: static content re-render', finalTemplate, this);

        if (finalTemplate === undefined) {
            const cTemplate = Jotun.compile(this._originalText);
            finalTarget.innerHTML = _cachedHeaderHTML + cTemplate(this.context);
        }
        else {
            finalTarget.innerHTML = _cachedHeaderHTML + Jotun.render(finalTemplate, this.context);
        }

        finalTarget.querySelectorAll('[data-bind-template]').forEach((el) => {
            el.cTemplate = Jotun.compile(el.innerHTML);
            el.innerHTML = '[LOADING]';
        });
        finalTarget.querySelectorAll('[element-type]').forEach((el) => {
            const cTemplate = Jotun.compile(el.innerHTML);
            const dataPath = el.dataset.bindTemplate ? el.dataset.bindTemplate : '';
            const elType = el.getAttribute('element-type');

            const newEl = document.createElement(elType);
            newEl.innerHTML = '[TEMPLATE]';
            newEl.setAttribute('data-bind-template', dataPath);
            newEl.cTemplate = cTemplate;
            
            el.parentNode.replaceChild(newEl, el);
        });

        if (finalTarget === this._root) {
            this.el = {};
            this._root.querySelectorAll('[auto-register]').forEach(el => {
                const key = el.id;
                this.el[key] = el;
            });

            this._rendered = true;
        }

        // Bind to local client options
        finalTarget.querySelectorAll('[data-option-key]').forEach(el => {
            // Load the value from localStorage
            const key = el.dataset.optionKey;
            const val = localStorage.getItem(`option-${key}`);
            if (val !== null) {
                el.value = val;
            }
            el.addEventListener("change", changeLocalOption);
        });

        this._renderDynamicContent();
    }

    _renderDynamicContent() {
        //console.log(`EL: renderDynamicContent`, this);
        if (!this._rendered) {
            // No reason to update content that doesn't exist yet
            console.warn('renderDynamicContent: element not rendered yet', this);
            return;
        }

        this._updatePageStateElements();

        const context = this.context;
        this._root.querySelectorAll('[data-bind-template]').forEach(el => {
            const pathVal = util.getPathValue(context, el.dataset.bindTemplate);
            el.innerHTML = el.cTemplate(pathVal);
        });

        if (context.value !== undefined) {
            this._root.querySelectorAll('[data-bind-value]').forEach(el => {
                // TODO: Attach complete context path here?
                //   Instead of recreating on save
                //   (or... not)
                const elementValue = util.getPathValue(context.value, el.dataset.bindValue);
                if (elementValue === undefined) {
                    console.warn(`Element value undefined: ${el.dataset.bindValue}`, el);
                }
                else {
                    util.setElementValue(el, elementValue);
                }
            });
        }
    }

    _updatePageStateElements() {
        this._root.querySelectorAll('[data-bind-page-state]').forEach(el => {
            const pageContext = el.dataset.bindPageState ? [...this._pageContext, el.dataset.bindPageState] : this._pageContext;
            el.pageContext = pageContext;
            //console.log('EL: bind page state', this._pageState, el.dataset.bindPageState, el);
            el.pageState = util.getPathValue(this._pageState, el.dataset.bindPageState);
        });
    }

    static initLocalOptions() {
        // TODO: List of options and defaults?
        // Set the current theme
        const initTheme = localStorage.getItem('option-theme');
        if (initTheme) {
            // TODO: data-bind-local-storage
            const el = document.querySelector('[data-option-key="theme"]');
            if (el) {
                el.value = initTheme;
            }
            document.documentElement.setAttribute(`data-theme`, initTheme);
        }
    }
}
window.customElements.define('cs-element', Element);

function changeLocalOption(e) {
    const el = e.target;
    const key = el.dataset.optionKey;
    const val = el.value;
    console.log(`set local option: ${key} = ${val}`);
    document.documentElement.setAttribute(`data-${key}`, val);
    localStorage.setItem(`option-${key}`, val);
}