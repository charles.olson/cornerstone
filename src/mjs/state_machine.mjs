import {BitSet} from "bitset";

import {Element} from "./element.mjs";
import {global} from "./element.mjs";
import {ElementState} from "./element_state.mjs";
import * as util from "./util.mjs";

export const defaultStateMachineDef = {
    'init': {
        'active': true,
        'class': ElementState,
        'config': {
            'template': 'cs_loading',
        }
    },
};

export class StateMachine extends Element {
    constructor(stateMachineDef) {
        super();

        this._isRunning = null;

        // The raw map of transition rules and state properties
        this._stateMachineDef = stateMachineDef;
        this._updateStatesHandle = null;

        // Shared variables between states
        this._shared = {};
    }

    onActivate() {
        super.onActivate();
        this._loadStateMachineDef();
        this._run();
    }

    get shared() {
        return this._shared;
    }

    _loadStateMachineDef() {
        console.log('SM: loadStateMachineDef', this._stateMachineDef);
        this._current = new BitSet();
        this._pending = new BitSet();

        // Map the state name to a local instance of the state data
        this._stateMap = {};
        this._activeStateMap = {};
        this._stateArray = [];
        this._stateTrackMap = {};

        // Instantiate the local state instances
        let stateIndex = 0;
        for (const[key, stateDef] of Object.entries(this._stateMachineDef)) {
            const stateClass = stateDef.class ? stateDef.class : ElementState;
            const stateTrack = stateDef.track ? stateDef.track : 'default';
            const stateInstance = new stateClass(key, stateIndex, this, stateDef.config, stateTrack);

            // State classes must be derived from the ElementState base class
            console.assert(stateInstance instanceof ElementState);

            this._stateMap[key] = stateInstance;
            this._activeStateMap[key] = false;
            this._stateArray.push(stateInstance);

            // Sort into tracks (typically for killing all states per track)
            this._getStateTrack(stateTrack).set(stateInstance);
            stateIndex++;
        }

        // Activate the default-active states
        for (const[key, state] of Object.entries(this._stateMachineDef)) {
            if (state.active) {
                this.spawnState(key);
            }
        }
    }

    _getStateTrack(trackName) {
        if (!this._stateTrackMap.hasOwnProperty(trackName)) {
            this._stateTrackMap[trackName] = new BitSet();
        }
        return this._stateTrackMap[trackName];
    }

    async _run() {
        if (this._isRunning) {
            return;
        }
        this._isRunning = true;
        console.log('SM: _run', this);

        const minFrameMS = 17;
        let iteration = 0;
        while (true) {
            console.log('SM: loop', iteration, this);
            const hasChanges = await this._updateStates();

            if (!hasChanges) {
                console.log('SM: Terminated', iteration, this);
                this._isRunning = false;
                return;
            }

            if (iteration > 20) {
                console.error('SM: Max iterations reached', iteration, this);
                return;
            }

            iteration++;
            await util.sleep(minFrameMS);
        }
    }

    async _updateStates() {
        console.log('SM: _updateStates', this);

        // Allow a subsequent update to be scheduled from any of the state logic
        this._updateStatesHandle = null;

        if (!this._active) {
            // @todo: this is probably fine (just getting scheduled right before deactivating an element)
            console.error('SM: _updateStates on inactive element', this);
            return;
        }

        //const hasChanges = !this._pending.equals(this._current);
        const stopSet = this._current.and(this._pending.not());
        const startSet = this._pending.and(this._current.not());

        // Copy pending into current
        this._current = this._pending.clone();

        const stopIndexList = stopSet.toArray();
        const startIndexList = startSet.toArray();
        const updateIndexList = this._current.toArray();
        
        console.log('SM: stop list', stopIndexList, this);
        for (const stateIndex of stopIndexList) {
            const state = this._stateArray[stateIndex];
            this._activeStateMap[state._key] = false;
            await state._stop();
            //state._stop();
        }
        //await Promise.all([

        for (const stateIndex of startIndexList) {
            const state = this._stateArray[stateIndex];
            this._activeStateMap[state._key] = true;
        }

        // Check if a new state changed the template
        let newTemplate;
        for (const stateIndex of startIndexList) {
            const state = this._stateArray[stateIndex];
            if (state._config && state._config.template) {
                newTemplate = state._config.template;
                break;
            }
        }

        if (newTemplate) {
            // Force re-render, even if the template is the same
            // (use undefined/null template to avoid re-render)
            console.log('SM: render template', newTemplate);
            this.setTemplateAndRender(newTemplate);
        }
        
        console.log('SM: start list', startIndexList, this);
        for (const stateIndex of startIndexList) {
            const state = this._stateArray[stateIndex];
            await state._start();
            //state._start();
        }

        // Updating doesn't really make sense without render frames
        console.log('SM: update list', updateIndexList, this);
        for (const stateIndex of updateIndexList) {
            const state = this._stateArray[stateIndex];
            await state._update();
            //state._update();
        }

        // TODO: Better inspection for dev/debug
        console.log('SM: state machine stats', this.getStats());

        const hasChanges = !this._pending.equals(this._current);
        return hasChanges;
    }

    _execFunction(fnName, target) {
        // see Element::_execFunction
        let execCount = 0;

        // Trigger the function by name, pass in this as the instigator
        if (this[fnName] !== undefined) {
            const fn = this[fnName].bind(this);
            fn(target);
            execCount++;
        }

        // Allow each active state to handle function calls (mainly to call click and change functions)
        const updateIndexList = this._current.toArray();
        for (const stateIndex of updateIndexList) {
            const state = this._stateArray[stateIndex];
            if (state._execFunction(fnName, target)) {
                execCount++;
            }
        }

        console.assert(execCount > 0, `${fnName} not a method of StateMachine or active states`, this);
    }

    spawnState(key) {
        console.log('SM: spawnState', key);
        const state = this._stateMap[key];
        if (!state) {
            console.error(`Undefined state: ${key}`);
            return;
        }

        this._pending.set(state._index, 1);
        this._requestUpdate();
    }

    killState(key) {
        console.log('SM: killState', key);
        const state= this._stateMap[key];
        if (!state) {
            console.error(`Undefined state: ${key}`);
            return;
        }

        this._pending.set(state._index, 0);
        this._requestUpdate();
    }

    killAllStates(track=undefined) {
        console.log('SM: killAllStates', track);
        if (track) {
            const trackSet = this._getStateTrack(track);
            this._pending = this._pending.and(trackSet.not());
        }
        else {
            this._pending.clear();
        }
        this._requestUpdate();
    }

    _requestUpdate() {
        // Start the state machine if necessary
        // (isRunning could be null or undefined before the state has been activated)
        if (this._isRunning !== false) {
            return;
        }

        const noChanges = this._pending.equals(this._current);
        if (noChanges) {
            return;
        }

        this._run();
    }

    getStats() {
        const stateStats = {};
        for (const state of this._stateArray) {
            stateStats[state._key] = state.getStats();
        }
        return stateStats;
    }
}
