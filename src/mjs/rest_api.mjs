import * as util from "./util.mjs";

const batchIntervalMS = 500;

export class RestAPI {
    constructor(url, config) {
        // Switch this to include the url directly in the constructor
        this._baseUrl = new URL(url);
        this._config = config;
        //let prefix = config.prefix ? config.prefix : '';
        //prefix = prefix.replaceAll(/^\/|\/$/g, '');
        //this._baseUrl = new URL(prefix, `${config.protocol}//${config.host}:${config.port}`);
        console.log('REST API BASE URL', this._baseUrl);
        // if (!this._config.headers) {
        //     this._config.headers = null;
        // }
    }

    async exec(request) {
        let requestPath = '';
        // if (request.path) {
        //     requestPath = request.path.replaceAll(/^\/|\/$/g, '');
        // }
        if (request.path) {
            requestPath = request.path.replaceAll(/^\//g, '');
        }
        
        const url = new URL(this._baseUrl);
        url.pathname = url.pathname.replaceAll(/\/$/g, '') + '/' + requestPath;

        // // TODO: Add to scheduler
        // // Exec immediately if most recent request was greater than some time ago
        // // Add to the batch otherwise
        // // todo: return fetch
        // // TODO: Allow overriding headers?
        // // Object.assign
        const fetchParams = {
            'method': request.method,
        };
        if (this._config.headers) {
            fetchParams.headers = this._config.headers;
        }
        if (request.body) {
            fetchParams.body = JSON.stringify(request.body);
        }

        console.log('REST API EXEC', url.href);
        const result = await fetch(url.href, fetchParams);
        const response = await result.json();
        return response;
    }

    async get(path, params=null) {
        return this.exec({
            'method': 'GET',
            'path': path,
            'params': params,
        });
    }
}