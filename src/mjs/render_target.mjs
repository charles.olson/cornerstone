import {Element, global} from "./element.mjs";
import * as util from "./util.mjs";

function crossInstanceId(obj, keyArray) {
    const valueArray = [];
    let hasMatch = false;
    for (const key of keyArray) {
        const val = util.getPathValue(obj, key, null);
        if (val === null) {
            valueArray.push('null');
        }
        else {
            hasMatch = true;
            valueArray.push(val);
        }
    }
    if (!hasMatch) {
        return null;
    }
    return valueArray.join('-');
}

export class RenderTarget extends Element {
    constructor() {
        super('cs_render_target');
        this._shadowRoot = this._root;
        this._initTemplateList();
    }

    static get observedAttributes() {
        return [...super.observedAttributes, 'instance-template', 'instance-keys'];
    }

    attributeChangedCallback(name, oldVal, newVal) {
        super.attributeChangedCallback(name, oldVal, newVal);

        if (name === 'instance-template') {
            this._instanceTemplate = newVal;
            console.log('SET INSTANCE TEMPLATE', newVal);
        }
        else if (name === 'instance-keys') {
            this._instanceKeys = JSON.parse(newVal);
        }
    }

    _initTemplateList() {
        if (this._templateList !== undefined) {
            return;
        }

        this._templateList = [];
        for (const el of this._slotChildren) {
            const template = el.getAttribute('template');
            if (!template) {
                continue;
            }

            let activeFilter = el.getAttribute('active-filter');
            if (activeFilter) {
                activeFilter = JSON.parse(activeFilter);
            }
            else {
                activeFilter = {};
            }

            const entry = {
                'filter': activeFilter,
                'template': template,
                'idx': this._templateList.length,
            };
            this._templateList.push(entry);
        }

        // Free the child elements
        this._slotChildren = null;
    }

    set pageState(jsonVal) {
        console.log('RT: set page state', this, jsonVal);
        // Copied from Element::set pageState
        // to keep early-out
        {
            const unchanged = util.deepEquals(this._pageState, jsonVal);
            if (unchanged) {
                console.log('RT: unchanged');
                return;
            }

            this._pageState = jsonVal;
            this.updateActive();
        }

        console.assert(this._rendered, 'setting pageState on unrendered element', this);
        console.assert(this._templateList !== undefined, 'templateList not initialized', this);

        // Render the first matching template
        for (const def of this._templateList) {
            if (this.evalFilter(def.filter)) {
                console.log(`RT: matches`, def);
                this._loadInstance(def);
                return;
            }
        }

        console.warn('RT: no active template found', this);
    }

    _loadInstance(def) {
        console.log('RT: loadInstance', def);
        if (this._currentInstanceIdx === def.idx) {
            console.log('RT: loadInstance: no change');
            return;
        }
        this._currentInstanceIdx = def.idx;

        // Deactivate all instances
        for (const el of this._shadowRoot.children) {
            el.active = false;
        }

        // Set the element root to the current instance
        this._root = this._getStaticInstance(def);
        this._root.pageState = this._pageState;
        this._root.pageContext = this._pageContext;
        this._root.active = true;
        console.log('RT: loadInstance set page state', this._pageState);

        this.dispatchEvent(new Event('change'));
    }

    _getStaticInstance(def) {
        console.log(`RT: get static instance`, def);
        if (!def.instance) {
            def.instance = document.createElement('cs-element');
            def.instance.pageState = this._pageState;
            def.instance.pageContext = this._pageContext;
            def.instance.id = `i${def.idx}`;
            def.instance.template = def.template;
            this._shadowRoot.appendChild(def.instance);
            console.log(`RT: created instance`, def);
        }
        return def.instance;
    }

    getInstanceState() {
        console.assert(false, 'TODO:')
        // const instanceState = [];
        // this._shadowRoot.querySelectorAll('render-instance').forEach(el => {
        //     instanceState.push(el);
        // });
        // return instanceState;
    }

    // _removeDiscarded() {
    //     console.log('REMOVE DISCARDED');
    //     const idList = [ this._getInstanceIdForPage(this._pageManager._currentPage) ];
    //     for (const page of this._pageManager._pageStack) {
    //         idList.push( this._getInstanceIdForPage(page) );
    //     }

    //     this._shadowRoot.querySelectorAll('render-instance').forEach(el => {
    //         console.log('REMOVE', el);
    //         if (!idList.includes(el.id)) {
    //             this._shadowRoot.removeChild(el);
    //         }
    //     });
    // }
};
window.customElements.define('cs-render-target', RenderTarget);